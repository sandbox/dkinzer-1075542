/**
 * @file Word count-down for twitter input field:
 */

if (Drupal.jsEnabled) {
  $(document).ready(
      function() {
        var tweetfield = $('.tweetfield').keyup(tweetfield_count_down).keypress(tweetfield_count_down).keypress().hover(
            tweetfield_highlight, tweetfield_dim).mouseout();
      });
}

/**
 * Count down function
 */
function tweetfield_count_down() {
  var default_count = ($(this).attr('tiny_url') == '1') ? 110 : 140;
  var left = default_count - $(this).val().length;
  if (left < 0) {
    left = 0;
    $(this).val(tweet);
  } else {
    tweet = $(this).val();
  }
  $('.tweetfield-counter').text(' - ' + left);
}

/**
 * Callback for hover in function
 */
function tweetfield_highlight() {
  var tweet_style_focusin = {
    'width' : '600px',
    'height' : '50px',
    'border' : '3px solid #cccccc',
    'padding' : '5px',
    'font-family' : 'Tahoma, sans-serif',
    'background-image' : 'url(bg.gif)',
    'background-position' : 'bottom right',
    'background-repeat' : 'no-repeat',
    'overflow' : 'hidden'
  };
  // @todo: move to css directive.
  $(this).css(tweet_style_focusin);
  $(this).focus();
}

/**
 * Callback for hover out function
 */
function tweetfield_dim() {
  var tweet_style_foucsout = {
    'width' : '600px',
    'height' : '50px',
    'border' : '1px solid #cccccc',
    'padding' : '5px',
    'font-family' : 'Tahoma, sans-serif',
    'background-image' : 'url(bg.gif)',
    'background-position' : 'bottom right',
    'background-repeat' : 'no-repeat',
    'overflow' : 'hidden'
  };
  // @todo: move to css directive.
  $(this).css(tweet_style_foucsout);
}
