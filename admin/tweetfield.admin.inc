<?php
/**
 * @file
 * Configuration form for Twitter
 */

/**
 * Form definition array for tweetfield Administrative panel.
 * @see system_settings_form();
 */
function tweetfield_admin_settings() {
  $form['tweetfield_twitter_consumer_key'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Consumer Key'),
    '#default_value' => variable_get('tweetfield_twitter_consumer_key', NULL),
    '#description'   => t('The Twitter application Consumer Key.'),
    '#required'      => TRUE,
  );

  $form['tweetfield_twitter_consumer_secret'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Consumer Secret'),
    '#default_value'  => variable_get('tweetfield_twitter_consumer_secret', NULL),
    '#description'    => t('The Twitter application Consumer Secret.'),
    '#required'       => TRUE,
  );
  
 $form['tweetfield_twitter_access_token'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Access Token'),
    '#default_value'  => variable_get('tweetfield_twitter_access_token', NULL),
    '#description'    => t('Get this from the "My Token" section of your Twitter application page.'),
    '#required'       => TRUE,
  );
  
 $form['tweetfield_twitter_token_secret'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Token Secret'),
    '#default_value'  => variable_get('tweetfield_twitter_token_secret', NULL),
    '#description'    => t('Get this from the "My Token" section of your Twitter application page.'),
    '#required'       => TRUE,
  );
  
  
 $form['tweetfield_twitter_application_url'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Twitter Application URL'),
    '#default_value'  => variable_get('tweetfield_twitter_application_url', NULL),
    '#description'    => t('Once you register the new application you may save a copy or the application URL here for easy access.'),
    '#required'       => FALSE,
  );

  $system_settings_form = system_settings_form($form);

  return $system_settings_form;
}