Welcome to tweetfield
--------------------------

This module defines a tweet field widget for CCK and handles tweeting the contents of the field that uses the widget.
